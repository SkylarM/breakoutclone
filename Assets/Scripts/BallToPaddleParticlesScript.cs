﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BalltoPaddleParticlesScript : MonoBehaviour
{

    ParticleSystem BalltoPaddleParticles;

    void Start()
    {
        BalltoPaddleParticles = GetComponent<ParticleSystem>();
    }

    void Update()
    {
        if (Input.GetButton("Jump"))
        {
            BalltoPaddleParticles.Play();
        }
    }
}
