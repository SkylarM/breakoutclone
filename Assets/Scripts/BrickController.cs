﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrickController : MonoBehaviour
{

    public UIScript ui;
    public GameObject healthPickup;
    public PowerUpController powerUpPrefab;
    public float chance = 0.1f;
    public int points = 10;
    public AudioSource ballHitPaddle;

    public Vector3 defaultPosition;
    public float xMovement = 2;

    void OnCollisionExit (Collision c)
    {
        if (c.gameObject.GetComponent<BallController>()) {
            ui.score += points;
            ui.bricksBroken++;
            gameObject.SetActive(false);
            if (Random.value < chance) {
                PowerUpController powerUp = Instantiate(powerUpPrefab) as PowerUpController;
                powerUp.transform.position = transform.position;
            }
        }
    }

    void Update()
    {
        transform.position = defaultPosition + Vector3.right * Mathf.Sin(Time.time) * xMovement;
    } 

}
