﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PaddleController : MonoBehaviour
{

    public float speed = 1;
    public int lives;
    bool isPlaying = false;
    public BallController ball;
    public UIScript uiScript;
    public static bool livesParticle = false;

    public AudioSource youLoseSound;
    public AudioSource youWinSound;
    public AudioSource gameOverVoice;
    public AudioSource LifeLostSound;

    public ParticleSystem lifeLostParticle;


    void Update()
    {


        float x = Input.GetAxis("Horizontal");
        Vector3 heading = new Vector3(x, 0, 0);
        Vector3 velocity = heading * speed;
        transform.position = transform.position + velocity;

    
    }
}    



