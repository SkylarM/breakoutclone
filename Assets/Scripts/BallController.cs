﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour
{

    public bool inPlay = false;
    public float speed = 10;
    public ParticleSystem brickParticles;
    private Rigidbody body;
    public float squishDuration = 0.1f;
    private float lastBounce;

    void Start()
    {
        body = GetComponent<Rigidbody>();
    }

    void OnCollisionEnter(Collision c)
    {
        if (c.contacts.Length > 0)
        {
            brickParticles.transform.position = c.contacts[0].point;
            brickParticles.transform.forward = c.contacts[0].normal;
            brickParticles.Play();
            lastBounce = Time.time;
            transform.up = c.contacts[0].normal;
            transform.localScale = new Vector3(1.3f, 0.5f, 1.3f);
        }

        if (c.gameObject.GetComponentInParent<PaddleController>())
        {
            float x = transform.position.x - c.gameObject.transform.position.x;
            body.velocity = new Vector3(x, 1, 0).normalized * speed;
        }
    }

    void Update()
    {
        if (inPlay)
        {
            Vector3 v = body.velocity;
            if (v.magnitude < 0.1f)
            {
                v = Vector3.up * speed;
            }
            if (Mathf.Abs(Vector3.Dot(v.normalized, Vector3.right)) > 0.9f)
            {
                v.y *= 1.1f;
            }
            body.velocity = v.normalized * speed;

            if (Time.time - lastBounce > squishDuration)
            {
                transform.up = v.normalized;
                transform.localScale = new Vector3(0.7f, 1.3f, 0.7f);
            }
        }
        else
        {
            body.velocity = Vector3.zero;
            transform.localScale = Vector3.one;
        }
    }
}
  

