﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Audio : MonoBehaviour
{
    public GameObject Paddle;

    public AudioSource BallRelease;
    public AudioSource PaddleHit;



    void Update()
    {
        if (Input.GetButtonDown("Jump"))
        {
            BallRelease.pitch = Random.Range(0.95f, 1.05f);
            BallRelease.Play();
        }
        

        if (Input.GetButtonDown("Fire1"))
        {
            PaddleHit.pitch = Random.Range(0.9f, 1.1f);
            PaddleHit.Play();
        }
    }
}
