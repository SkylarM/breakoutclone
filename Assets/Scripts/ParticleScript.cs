﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleScript : MonoBehaviour
{

    public PaddleController Paddle;


    ParticleSystem puff;

    void Start()
    {
        puff = GetComponent<ParticleSystem>();
    }

    void Update()
    {
        if (PaddleController.livesParticle == true) 
        {
            puff.Play();
            PaddleController.livesParticle = false;
        }
    }
}