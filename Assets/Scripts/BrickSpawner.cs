﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrickSpawner : MonoBehaviour
{

    public GameObject TieFighterPrefab;


    void Start()
    {
        for (int i = 0; i < 6; i++)
        {
            for (int j = 0; j < 4; j++)
            {
                GameObject g = Instantiate(TieFighterPrefab);
                g.transform.position = new Vector3((float)i * 12f - 30f, (float)j * 6f - -8f, 2.6f);
                MeshRenderer r = g.GetComponentInChildren<MeshRenderer>();
                r.material.color = Random.ColorHSV(0, 0, 0, 0, 0, .2f);
            }
        }
    }
}