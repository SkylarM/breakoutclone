﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpController : MonoBehaviour
{

    public float tumbleSpeed = 10;
    public Vector3 normalSize = new Vector3(4, 1, 1);
    public Vector3 smallSize = new Vector3(1, 1, 1);
    public Vector3 largeSize = new Vector3(8, 1, 1);
    private Rigidbody body;

    void Start()
    {
        body = GetComponent<Rigidbody>();
        body.angularVelocity = Random.insideUnitSphere * tumbleSpeed;
        body.velocity = Vector3.up * -3;
    }

    void Update()
    {
        if (transform.position.y < 50)
        {
            gameObject.SetActive(false);
        }
    }

    void OnTriggerEnter(Collider c)
    {
        PaddleController paddle = c.gameObject.GetComponentInParent<PaddleController>();
        if (paddle != null)
        {
            Vector3[] sizes = new Vector3[] { smallSize, normalSize, largeSize };
            paddle.GetComponentInChildren<BoxCollider>().transform.localScale = sizes[Random.Range(0, sizes.Length)];
            gameObject.SetActive(false);
        }
    }
}
