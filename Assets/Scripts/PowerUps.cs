﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUps : MonoBehaviour {

    public GameObject pickupEffect;

    private Rigidbody rb;
    private AudioSource HealthSound;

    void Awake()
    {

        rb = GetComponent<Rigidbody>();
        HealthSound = GetComponent<AudioSource>();
    }

    void OnCollisionExit(Collision c)
    {
        BrickController brick = c.gameObject.GetComponent<BrickController>();

        gameObject.SetActive(false);

        if (brick != null)
        {

            HealthSound.Play();
        }
        }
    }


