﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIScript : MonoBehaviour
{

    public Text scoreText;
    public Text livesText;
    public Text highScoreText;
    public Text gameOverText;

    public int easyRows = 2;
    public int hardRows = 12;
    public int easyColumns = 3;
    public int hardColumns = 16;
    public Vector2 easySpacing = new Vector2(6, 4);
    public Vector2 hardSpacing = new Vector2(2, 1);

    public int level = 0;
    public int maxLevel = 100;

    private int rows = 5;
    private int columns = 6;
    private Vector2 spacing = new Vector2(4, 2);
    public float yOffset = 3;
    public BrickController brickPrefab;

    public PaddleController paddle;
    public BallController ball;

    public int startLives = 3;
    private int lives;
    public int score;
    public int bricksBroken;

    private bool gameOver = false;
    private BrickController[] bricks = new BrickController[0];

    void Start()
    {
        gameOver = false;
        ball.inPlay = false;
        if (level <= 0)
        {
            lives = startLives;
            score = 0;
        }

        float difficulty = (float)level / (float)maxLevel;
        rows = (int)Mathf.Lerp(easyRows, hardRows, difficulty);
        columns = (int)Mathf.Lerp(easyColumns, hardColumns, difficulty);
        spacing = Vector2.Lerp(easySpacing, hardSpacing, difficulty);

        bricksBroken = 0;
        BrickController[] lastBricks = bricks;
        bricks = new BrickController[rows * columns];
        for (int i = 0; i < bricks.Length; i++)
        {
            if (i < lastBricks.Length)
            {
                BrickController brick = lastBricks[i];
                brick.gameObject.SetActive(true);
                bricks[i] = brick;
            }
            else
            {
                BrickController brick = Instantiate(brickPrefab) as BrickController;
                brick.ui = this;
                bricks[i] = brick;
            }
        }

        for (int x = 0; x < columns; x++)
        {
            for (int y = 0; y < rows; y++)
            {
                BrickController brick = bricks[y * columns + x];
                brick.defaultPosition = new Vector3(
                    (x - (float)(columns - 1) * 0f) * spacing.x,
                    y * spacing.y + yOffset,
                    0
                );
            }
        }

        gameOverText.text = "";
        ball.transform.parent = paddle.transform;
        ball.transform.localPosition = Vector3.up * 2;
        highScoreText.text = "HighScore: " + PlayerPrefs.GetInt("HighScore", 0);
    }

    void Update()
    {
        if (!gameOver && ball.inPlay && ball.transform.position.y < paddle.transform.position.y - 2)
        {
            ball.inPlay = false;
            ball.transform.parent = paddle.transform;
            ball.transform.localPosition = Vector3.up * 2;

            lives--;
            livesText.text = "Lives: " + lives;
            if (lives == 0)
            {
                gameOver = true;
                gameOverText.text = "You Lose";
                level = 0;
            }
        }
        else if (!gameOver && !ball.inPlay && Input.GetButtonDown("Jump"))
        {
            ball.inPlay = true;
            ball.transform.parent = null;
        }

        if (!gameOver && bricksBroken == rows * columns)
        {
            gameOver = true;
            gameOverText.text = "You Win";
            level++;
        }

        scoreText.text = "Score: " + score;
        int highScore = PlayerPrefs.GetInt("HighScore", 0);
        if (score > highScore)
        {
            highScore = score;
            PlayerPrefs.SetInt("HighScore", highScore);
            PlayerPrefs.Save();
            highScoreText.text = "HighScore: " + highScore;
        }

        if (gameOver && Input.GetButtonUp("Jump"))
        {
            Start();
        }
    }
}
